use crate::prelude::*;

property!(
    /// `FontSize` describes the font size of a text element.
    FontSize(f64)
);
