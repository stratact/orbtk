use crate::prelude::*;

property!(
    /// `RowSpan` describes the row span of a widget on the `Grid`.
    RowSpan(usize)
);